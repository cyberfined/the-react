The React is crossplatform minimal top-down shooter, written in c, SDL, OpenGL.

COMPILE
To compile it, you must execute make.

DEPENDENCIES
SDL2

RUN
./bin/react screen_width screen_height

CONTROLS
W - move forward
A - strafe left
D - strafe right
S - strafe down
Left, Middle, Right mouse buttons - shoot

GAMEPLAY
To leave the level, you must kill all enemies on it and go to end of level.

MAP FILES FORMAT
maps for react is a regular text files.
For example: w,100,100,0,0,1;
w - wall;
100 - x coord;
100 - y coord;
0 - red color;
0 - green color;
1 - blue color;

For example: e,80,25,hexenemy;
e - enemy;
80 - x coord;
25 - y coord;
hexenemy - filename with enemy options;

For example: b,12,32,speedup;
b - bonus;
12 - x coord;
32 - y coord;
speedup - filename with bonus options;

For example: f,300,0,500,100;
f - end of the level;
300 - x coord;
0 - y coord;
500 - width;
100 - height;

For example: p,950,150;
p - player start;
950 - x coord;
150 - y coord;

ENEMY FILES FORMAT
v,0,0,0,1,0,20,0,0,1,0,20,20,0,1,0,0,20,0,1,0;w,20;h,20;l,10;s,2;r,2;g,7;d,20;

v,x1,y1,red,green,blue,x2,y2,red,green,blue,...; - points(LINE_LOOP) to draw enemy
w,20; - width;
h,20; - height;
l,10; - lifes of enemy;
s,2; - speed of enemy;
r,2; - enemy bullet radius;
g,7; - enemy bullet speed;
d,20; - enemy shoot duration;

BONUS FILES FORMAT
v,0,10,1,0,0,0,0,1,0,0,5,0,1,0,0,5,5,1,0,0,0,5,1,0,0,5,10,1,0,0,0,5,1,0,0;w,5;h,10;f,radiusup;d,400;a1,6;a2,10;

v,x1,y1,red,green,blue,x2,y2,red,green,blue,...; - points(LINE_LOOP) to draw bonus
w,5; - width;
h,10; - height;
f,radiusup; - name of function, which contains in bonus_func.c
	Function prototype: void func_name(Player *p, float data);
d,400; - duration of second function activate;
a1,6; - argument for first function activate;(aka data);
a2,10; - argument for second function activate(aka data);
